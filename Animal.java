
public class Animal {

  private String name = "animal";
  private int age = 1;
  private int lifePoints = 100;
  private int attack = 4;

	public Animal(){}

  public Animal(String name, int age, int lifePoints, int attack){
    this.name = name;
    this.age = age;
    this.lifePoints = lifePoints;
    this.attack = attack;
    System.out.println("Constructing an Animal");
  }

	public String getName() {
		return name;
	}

  public int getAge() {
		return age;
	}

  public String speak() {
    return "blabla";
  }
}

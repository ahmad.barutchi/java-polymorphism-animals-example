
public class Cat extends Animal {

  private String meow = "meow";

  public Cat(){}

  public Cat(String name, int age, int lifePoints, int attack, String meow){
    super(name, age,lifePoints, attack);
    this.meow = meow;
    System.out.println("Constructing a Cat");
  }

  public Cat(String name, int age, int lifePoints, int attack){
    super(name, age,lifePoints, attack);
    System.out.println("Constructing a Cat");
  }

  public String speak() {
		return this.meow;
	}
}

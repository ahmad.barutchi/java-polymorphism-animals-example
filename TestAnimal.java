
public class TestAnimal{

  // main class
  public static void main(String[] args)
  {
    // creating Cats and Dogs
    Animal bou = new Cat("bou", 12, 100, 3, "MOOOO");
    Animal spark = new Dog("spark", 2, 100, 7);
    Animal dogy = new Dog("spooky", 8, 200, 9, "wuff wuff");

  // printing Cats and Dogs informations
		System.out.println("The cat name : " + bou.getName()+ ", says: " + bou.speak());
    System.out.println("The Dog name : " + spark.getName()+ ", says: " + spark.speak());
    System.out.println("The Dog name : " + dogy.getName()+ ", says: " + dogy.speak());

    // changing a dog into cat
    dogy = new Cat();
    System.out.println("The new cat name : " + dogy.getName()+ ", says: " + dogy.speak());
  }
}

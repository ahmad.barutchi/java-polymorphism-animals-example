
public class Dog extends Animal {

  private String bark = "ouaf ouaf";

  public Dog(){}

  public Dog(String name, int age, int lifePoints, int attack, String bark){
    super(name, age,lifePoints, attack);
    this.bark = bark;
    System.out.println("Constructing a Dog");
  }

  public Dog(String name, int age, int lifePoints, int attack){
    super(name, age,lifePoints, attack);
    System.out.println("Constructing a Dog");
  }

  public String speak() {
		return this.bark;
	}
}
